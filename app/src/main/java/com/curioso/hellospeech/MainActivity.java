package com.curioso.hellospeech;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SpeechRecognizer mySr;
    private TextView myTv;
    private TextView recognizerTv;
    private static final String TAG = "MainActivity";
    private int RECORD_PERMISSION_CODE = 1;
    private int READ_PERMISSION_CODE = 2;
    private int WRITE_PERMISSION_CODE = 3;
    public static final String PREF_LANGUAGE = "language";
    public static final String PREF_WAKE = "wake";
    public static final String PREF_RECOGNIZER = "recognizer";
    public static SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED){
            requestRecordPermission();
        }
        if(ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            requestReadPermission();
        }
        if(ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            requestWritePermission();
        }

        readPreferences();
        myTv = findViewById(R.id.textViewResults);
        recognizerTv = findViewById(R.id.textViewRecognizer);

        recognizerTv.setText("RECOGNIZER: "+prefs.getString(PREF_RECOGNIZER,"default"));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(prefs.getBoolean(PREF_WAKE,false)){
                    if(mySr!=null){
                        mySr.destroy();
                    }else{
                        Toast.makeText(MainActivity.this,"Speech Recognizer not initialized", Toast.LENGTH_SHORT).show();
                    }
                    startActivity(new Intent(MainActivity.this, ListeningActivity.class));
                }else{
                    runSr();
                }


            }
        });

        initSr();
    }

    private void requestWritePermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            new AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("Permission is required for Speech Recognizer")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_PERMISSION_CODE);

                        }
                    })
                    .create().show();

        } else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_PERMISSION_CODE);
        }
    }

    private void requestReadPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
            new AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("Permission is required for Speech Recognizer")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_PERMISSION_CODE);

                        }
                    })
                    .create().show();

        } else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_PERMISSION_CODE);
        }
    }

    private void requestRecordPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)){
            new AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("Permission is required for Speech Recognizer")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, RECORD_PERMISSION_CODE);

                        }
                    })
                    .create().show();

        } else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, RECORD_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RECORD_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"PERMISSION IS REQUIRED",Toast.LENGTH_SHORT).show();
                requestRecordPermission();
            }
        }
    }

    public void readPreferences (){
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void runSr(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS,1);
        if(mySr!=null){
            mySr.startListening(intent);
        }
    }

    private void initSr() {

        switch (prefs.getString(PREF_RECOGNIZER,"default")){
            case "google":
                googleSr();
                myTv.setText("google not implemented");
                break;

            case "azure":
                azureSr();
                myTv.setText("azure not implemented");
                break;

            case "alexa":
                alexaSr();
                myTv.setText("alexa not implemented");
                break;

            case "sphinx":
                sphinxSr();
                myTv.setText("sphinx not implemented");
                break;
            

            default:
                defaultSr();

        }
    }

    private void sphinxSr() {

    }

    private void alexaSr() {

    }

    private void azureSr() {

    }

    private void googleSr() {

    }

    private void defaultSr(){
        mySr = SpeechRecognizer.createSpeechRecognizer(this);
        myTv.setText("SR initialized");
        mySr.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {
                myTv.setText("ReadyForSpeech");

            }

            @Override
            public void onBeginningOfSpeech() {
                myTv.setText("Listening ...");

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {
                myTv.setText("Listening ended");

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onResults(Bundle results) {
                myTv.setText("Results received");
                List<String>resultsList = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                processSr(resultsList.get(0));

            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });

    }

    private void processSr(String resultString) {
        resultString=resultString.toLowerCase();
//        if(resultString.contains("si")){
//            speak("ok, afirmativo");
//        }
//        if(resultString.contains("no")){
//            speak("cancelare, negativo");
//
//        }
//        else{
//            speak("responde si ou no");
//        }
        myTv.setText(resultString);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mySr!=null){
            mySr.destroy();
        }

    }
    @Override
    protected void onResume(){
        super.onResume();
        readPreferences();
        initSr();
        runSr();

    }
}
