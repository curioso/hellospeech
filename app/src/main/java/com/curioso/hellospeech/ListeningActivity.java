package com.curioso.hellospeech;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import javax.crypto.Mac;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

public class ListeningActivity extends AppCompatActivity implements RecognitionListener {
    private static final String LOG_TAG = ListeningActivity.class.getName();
    private static final String WAKEWORD_SEARCH = "wake";
    private static int sensitivity = 20;
    private SpeechRecognizer mRecognizer;
    public String[] commands;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listening);
        TextView wordTv = findViewById(R.id.textView3);
        wordTv.setText(R.string.wake_word);
        final TextView sensTV = findViewById(R.id.sensTextView);
        sensTV.setText(String.valueOf(sensitivity));
        final SeekBar sensBar = findViewById(R.id.bar);
        sensBar.setProgress(sensitivity);
        sensBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                sensTV.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                sensitivity = sensBar.getProgress();
                sensTV.setText(String.valueOf(sensitivity));
                mRecognizer.removeListener(ListeningActivity.this);
                mRecognizer.stop();
                mRecognizer.shutdown();
                setup();

            }
        });


    }
    @Override
    protected void onResume(){
        super.onResume();
        setup();
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(mRecognizer!=null){
            mRecognizer.removeListener(this);
            mRecognizer.cancel();
            mRecognizer.shutdown();
            Log.d(LOG_TAG, "Recognizer shutdown");
        }
    }

    private void setup(){

        try{
            final Assets assets = new Assets (ListeningActivity.this);
            final File assetDir = assets.syncAssets();
            mRecognizer = SpeechRecognizerSetup.defaultSetup()
                    .setAcousticModel(new File(assetDir, "en-us-ptm"))
                    .setDictionary(new File(assetDir, "custom.dict"))
                    //.setKeywordThreshold(Float.valueOf("1e-"+ 2 * sensitivity))
                    .getRecognizer();
            mRecognizer.addKeyphraseSearch(WAKEWORD_SEARCH, getString(R.string.wake_word));
            mRecognizer.addListener(this);
            mRecognizer.startListening(WAKEWORD_SEARCH);

            Log.d(LOG_TAG, "... listening");
        } catch (IOException e){
            Log.e(LOG_TAG, e.toString());
        }
    }
    @Override
    public void onBeginningOfSpeech(){
        if(getSupportActionBar() != null){
            getSupportActionBar().setSubtitle("LISTENING");

        }
    }

    @Override
    public void onEndOfSpeech(){
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle("");
        }
    }

    @Override
    public void onPartialResult(final Hypothesis hypothesis) {
        if (hypothesis != null) {
            final String text = hypothesis.getHypstr();
            Log.d(LOG_TAG, "on partial: " + text);
            if (text.equals(getString(R.string.wake_word))) {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setSubtitle("");
                }
                startActivity(new Intent(this, MainActivity.class));
            }else{
                for (String c : commands){
                    if(text.equals(c)){
                        Toast.makeText(this,"RECOGNIZED:"+c,Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    @Override
    public void onResult(final Hypothesis hypothesis) {
        if (hypothesis != null) {
            Log.d(LOG_TAG, "on Result: " + hypothesis.getHypstr() + " : " + hypothesis.getBestScore());
            if (getSupportActionBar() != null) {
                getSupportActionBar().setSubtitle("");
            }
        }
    }
    @Override
    public void onError(final Exception e) {
        Log.e(LOG_TAG, "on Error: " + e);
    }

    @Override
    public void onTimeout() {
        Log.d(LOG_TAG, "on Timeout");
    }
}
